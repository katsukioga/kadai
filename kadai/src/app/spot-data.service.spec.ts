import { TestBed } from '@angular/core/testing';
import { SpotDataService } from './spot-data.service';

describe('SpotDataService', () => {
  let service: SpotDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SpotDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

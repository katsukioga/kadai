import { Injectable } from '@angular/core';
import { SpotData } from './spotData';
import { SPOTDATA } from './mock-spot';

import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpotDataService {

  constructor() {}
  getSpotData(): Observable<SpotData[]> {
    return of(SPOTDATA);
  }
  setSearchWord(term:string){
    console.log(term);
  }
}

import { Component, OnInit } from '@angular/core';
import {MapMouseEvent} from "mapbox-gl";
import {LngLat, MapLayerMouseEvent } from 'mapbox-gl';
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  constructor(private snackBar: MatSnackBar) {}
  ngOnInit(): void {}
  lngLat: number[];

  /**
   *   コンソールに地図クリック時の座標を取得
   */
  onClick(event:  MapMouseEvent) {
      console.log('onClick', event.lngLat);
  }

  /**
   *   メッセージを表示
   */
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  /**
   *   緯度経度を設定
    */
  setLngLat(arrayLngLat: number[]) {
    this.lngLat=arrayLngLat;
  }

  /**
   *   緯度経度を取得
   */
  getLngLat(): number[] {
    return this.lngLat;
  }

  clear(): void{
    this.lngLat=[];
  }
}

import {SpotData} from './spotData';

export const SPOTDATA: SpotData[] = [
  { id: 1, name: 'スポット1', address: '東京都文京区本駒込1', lnglat:[139.0,35.0]},
  { id: 2, name: 'スポット2', address: '東京都文京区本駒込2', lnglat:[139.2,35.0]},
  { id: 3, name: 'スポット3', address: '東京都文京区本駒込3', lnglat:[139.4,35.0]},
  { id: 4, name: 'スポット4', address: '東京都文京区本駒込4', lnglat:[139.6,35.0]},
  { id: 5, name: 'スポット5', address: '東京都文京区本駒込5', lnglat:[139.8,35.0]}
];

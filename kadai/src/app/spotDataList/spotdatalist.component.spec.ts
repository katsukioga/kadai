import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpotDataListComponent } from './spotdatalist.component';

describe('SpotDataListComponent', () => {
  let component: SpotDataListComponent;
  let fixture: ComponentFixture<SpotDataListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpotDataListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpotDataListComponent;
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

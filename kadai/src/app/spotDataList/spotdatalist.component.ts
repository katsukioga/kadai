import {Component, OnInit} from '@angular/core';
import {SpotData} from '../spotData';
import {SpotDataService} from '../spot-data.service';
import { Observable, Subject } from 'rxjs';
import {MapMouseEvent} from "mapbox-gl";
import { MapComponent } from '../map/map.component';

@Component({
  selector: 'app-spotdatalist',
  templateUrl: './spotdatalist.component.html',
  styleUrls: ['./spotdatalist.component.scss']
})
export class SpotDataListComponent implements OnInit {
  private searchTerms = new Subject<string>();
  selectedSpot: SpotData;
  spotDatas : SpotData[];
  lnglat: number[];

  key = null;
  constructor(
    private spotDaraService: SpotDataService,
    private mapComponent: MapComponent
  ) { }

  ngOnInit(): void {}

  /**
   *　選択したリストのスポット情報を設定
   */
  onSelect(spotData: SpotData): void {
    this.selectedSpot = spotData;
    this.mapComponent.setLngLat(spotData.lnglat);
  }

  /**
   *　スポットデータを取得
   */
  getSpotData(): void {
    this.spotDaraService.getSpotData()
      .subscribe(spotDatas => this.spotDatas = spotDatas);
  }

  /**
   *　検索文字をコンソールに出力
   */
  search(term: string): void {
    this.getSpotData();
    this.spotDaraService.setSearchWord(term);
  }

  lngLatClear():void{
    this.mapComponent.clear();
  }
}

export interface SpotData {
  id: number;
  name: string;
  address: string;
  lnglat: [number,number];
}
